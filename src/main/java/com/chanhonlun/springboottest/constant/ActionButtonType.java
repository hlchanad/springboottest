package com.chanhonlun.springboottest.constant;

public enum ActionButtonType {

    REDIRECT, DELETE
}
