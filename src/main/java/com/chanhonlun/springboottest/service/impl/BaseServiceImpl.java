package com.chanhonlun.springboottest.service.impl;

import com.chanhonlun.springboottest.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseServiceImpl implements BaseService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
}
